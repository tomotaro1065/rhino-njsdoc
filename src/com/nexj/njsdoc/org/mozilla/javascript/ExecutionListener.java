package com.nexj.njsdoc.org.mozilla.javascript;

/**
 * 
 */
public interface ExecutionListener {

    /**
     * lhs[slotName] = rhs
     * 
     * @param lhs The receiver
     * @param slotName The slotName - String or Integer. May be null if unknown
     * @param rhs The value that is being assigned
     * @param codeLocation The code location that this assignment is occurring at
     */
    public void slotAssigned(Object lhs, Object slotName, Object rhs, CodeLocation codeLocation);

    /**
     * Store a comment location
     * @param codeLocation The location
     * @param value The text
     */
    public void storeComment(String commentText, CodeLocation codeLocation);
}
